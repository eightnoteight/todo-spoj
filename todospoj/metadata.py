# -*- coding: utf-8 -*-


package = "todospoj"
project = "todospoj"
version = "0.1al"
description = "for indexing and tagging spoj problems"
authors = ["Srinivas Devaki(eightnoteight)"]
emails = ["mr.eightnoteight@gmail.com"]
license = "Apache"
copyright = "2014" + ', '.join(authors)
url = "http://bitbucket.com/eightnoteight/todo-spoj"
